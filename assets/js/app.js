$(function(){

	console.log("salut");

	getMarkdownContent();

})

function getMarkdownContent() {
	// Récupération du markdown externe
	let urlMD = window.location.hash.substring(1); // Récupère l'URL du hashtag sans le #

	if (urlMD !== "") {
		// Gestion des fichiers hébergés sur github
		if (urlMD.startsWith("https://github.com")) {
			urlMD = urlMD.replace(
				"https://github.com",
				"https://raw.githubusercontent.com"
			);
			urlMD = urlMD.replace("/blob/", "/");
		}
		// Gestion des fichiers hébergés sur codiMD
		if (
			urlMD.startsWith("https://codimd") &&
			urlMD.indexOf("download") === -1
		) {
			urlMD =
				urlMD
					.replace("?edit", "")
					.replace("?both", "")
					.replace("?view", "")
					.replace(/#$/, "") + "/download";
		}
		// Gestion des fichiers hébergés via Hedgedoc
		if (
			urlMD.includes("hedgedoc") &&
			urlMD.indexOf("download") === -1
		) {
			urlMD =
				urlMD
					.replace("?edit", "")
					.replace("?both", "")
					.replace("?view", "")
					.replace(/#$/, "") + "/download";
		}

		// Récupération du contenu du fichier
		
		let md = fetch(urlMD)
			.then((response) => response.text())
			.then((data) => {
				afficherdata(data);
			}) 
	
			.catch((error) => console.error(error));
		

	} 

}

function afficherdata(data) {
	console.log(data);
	$('#contenuMd').html(data);
}

